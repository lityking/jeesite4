/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.TreeService;
import com.jeesite.modules.recordmanage.entity.RecordCatalogue;
import com.jeesite.modules.recordmanage.dao.RecordCatalogueDao;

/**
 * 档案目录Service
 * @author system
 * @version 2018-06-11
 */
@Service
@Transactional(readOnly=true)
public class RecordCatalogueService extends TreeService<RecordCatalogueDao, RecordCatalogue> {
	
	/**
	 * 获取单条数据
	 * @param recordCatalogue
	 * @return
	 */
	@Override
	public RecordCatalogue get(RecordCatalogue recordCatalogue) {
		return super.get(recordCatalogue);
	}
	
	/**
	 * 查询列表数据
	 * @param recordCatalogue
	 * @return
	 */
	@Override
	public List<RecordCatalogue> findList(RecordCatalogue recordCatalogue) {
		return super.findList(recordCatalogue);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param recordCatalogue
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(RecordCatalogue recordCatalogue) {
		super.save(recordCatalogue);
	}
	
	/**
	 * 更新状态
	 * @param recordCatalogue
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(RecordCatalogue recordCatalogue) {
		super.updateStatus(recordCatalogue);
	}
	
	/**
	 * 删除数据
	 * @param recordCatalogue
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(RecordCatalogue recordCatalogue) {
		super.delete(recordCatalogue);
	}
	
}