/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recordmanage.entity.Staff;
import com.jeesite.modules.recordmanage.dao.StaffDao;

/**
 * staffService
 * @author system
 * @version 2018-06-08
 */
@Service
@Transactional(readOnly=true)
public class StaffService extends CrudService<StaffDao, Staff> {
	
	/**
	 * 获取单条数据
	 * @param staff
	 * @return
	 */
	@Override
	public Staff get(Staff staff) {
		return super.get(staff);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param staff
	 * @return
	 */
	@Override
	public Page<Staff> findPage(Page<Staff> page, Staff staff) {
		return super.findPage(page, staff);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param staff
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Staff staff) {
		super.save(staff);
	}
	
	/**
	 * 更新状态
	 * @param staff
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Staff staff) {
		super.updateStatus(staff);
	}
	
	/**
	 * 删除数据
	 * @param staff
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Staff staff) {
		super.delete(staff);
	}
	
}