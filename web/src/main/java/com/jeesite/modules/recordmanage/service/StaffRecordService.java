/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recordmanage.entity.StaffRecord;
import com.jeesite.modules.recordmanage.dao.StaffRecordDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * staffRecordService
 * @author system
 * @version 2018-06-11
 */
@Service
@Transactional(readOnly=true)
public class StaffRecordService extends CrudService<StaffRecordDao, StaffRecord> {
	
	/**
	 * 获取单条数据
	 * @param staffRecord
	 * @return
	 */
	@Override
	public StaffRecord get(StaffRecord staffRecord) {
		return super.get(staffRecord);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param staffRecord
	 * @return
	 */
	@Override
	public Page<StaffRecord> findPage(Page<StaffRecord> page, StaffRecord staffRecord) {
		return super.findPage(page, staffRecord);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param staffRecord
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(StaffRecord staffRecord) {
		super.save(staffRecord);
		// 保存上传附件
		FileUploadUtils.saveFileUpload(staffRecord.getId(), "staffRecord_file");
	}
	
	/**
	 * 更新状态
	 * @param staffRecord
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(StaffRecord staffRecord) {
		super.updateStatus(staffRecord);
	}
	
	/**
	 * 删除数据
	 * @param staffRecord
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(StaffRecord staffRecord) {
		super.delete(staffRecord);
	}
	
}