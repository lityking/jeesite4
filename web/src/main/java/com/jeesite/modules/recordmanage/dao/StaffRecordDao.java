/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recordmanage.entity.StaffRecord;

/**
 * staffRecordDAO接口
 * @author system
 * @version 2018-06-11
 */
@MyBatisDao
public interface StaffRecordDao extends CrudDao<StaffRecord> {
	
}