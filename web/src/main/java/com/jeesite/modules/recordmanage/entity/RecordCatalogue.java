/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.entity;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.entity.TreeEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 档案目录Entity
 * @author system
 * @version 2018-06-11
 */
@Table(name="record_catalogue", alias="a", columns={
		@Column(name="tree_code", attrName="treeCode", label="目录编码", isPK=true),
		@Column(includeEntity=TreeEntity.class),
		@Column(name="tree_name", attrName="treeName", label="节点名称", queryType=QueryType.LIKE, isTreeName=true),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.tree_sorts, a.tree_code"
)
public class RecordCatalogue extends TreeEntity<RecordCatalogue> {
	
	private static final long serialVersionUID = 1L;
	private String treeCode;		// 目录编码
	private String treeName;		// 节点名称
	
	public RecordCatalogue() {
		this(null);
	}

	public RecordCatalogue(String id){
		super(id);
	}
	
	@Override
	public RecordCatalogue getParent() {
		return parent;
	}

	@Override
	public void setParent(RecordCatalogue parent) {
		this.parent = parent;
	}
	
	public String getTreeCode() {
		return treeCode;
	}

	public void setTreeCode(String treeCode) {
		this.treeCode = treeCode;
	}
	
	@NotBlank(message="节点名称不能为空")
	@Length(min=0, max=255, message="节点名称长度不能超过 255 个字符")
	public String getTreeName() {
		return treeName;
	}

	public void setTreeName(String treeName) {
		this.treeName = treeName;
	}
	
}