/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;
import com.jeesite.modules.file.entity.FileUpload;
import com.jeesite.modules.sys.entity.Company;
import com.jeesite.modules.sys.entity.Office;

/**
 * staffRecordEntity
 * @author system
 * @version 2018-06-11
 */
@Table(name="staff_record", alias="a", columns={
		@Column(name="id", attrName="id", label="编号", isPK=true),
		@Column(name="staff_id", attrName="staff.staffCode", label="职工工号"),
		@Column(name="cat_id", attrName="recordCatalogue.treeCode", label="目录id"),
		@Column(name="file_name", attrName="fileName", label="文件名", queryType=QueryType.LIKE),
		@Column(name="file_path", attrName="filePath", label="路径"),
		@Column(name="file_page", attrName="filePage", label="文件页码"),
		@Column(name="file_date", attrName="fileDate", label="材料制成时间"),
		@Column(includeEntity=DataEntity.class),
	},joinTable={
			 @JoinTable(type=Type.JOIN, entity=Staff.class, alias="s",
			            on="s.staff_code = a.staff_Id ",
			            columns={@Column(includeEntity=Staff.class)}),
			 @JoinTable(type=Type.JOIN, entity=RecordCatalogue.class, alias="r",
			 on="r.tree_code = a.cat_Id ",
			 columns={@Column(includeEntity=RecordCatalogue.class)}),
			
	}, orderBy="a.update_date DESC"
)
public class StaffRecord extends DataEntity<StaffRecord> {
	
	private static final long serialVersionUID = 1L;
	private Staff staff;
	private RecordCatalogue recordCatalogue;
	private String fileName;		// 文件名
	private String filePath;		// 路径
	private Integer filePage;		// 文件页码
	private Date fileDate;		// 材料制成时间
	
	
	private List<FilePage> files;
	
	

	public List<FilePage> getFiles() {
		return files;
	}

	public void setFiles(List<FilePage> files) {
		this.files = files;
	}

	public StaffRecord() {
		this(null);
	}

	public StaffRecord(String id){
		super(id);
	}
	
	
	@NotNull(message="员工不能为空")
	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	@NotNull(message="目录不能为空")
	public RecordCatalogue getRecordCatalogue() {
		return recordCatalogue;
	}

	public void setRecordCatalogue(RecordCatalogue recordCatalogue) {
		this.recordCatalogue = recordCatalogue;
	}

	@Length(min=0, max=200, message="文件名长度不能超过 200 个字符")
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Length(min=0, max=200, message="路径长度不能超过 200 个字符")
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	public Integer getFilePage() {
		return filePage;
	}

	public void setFilePage(Integer filePage) {
		this.filePage = filePage;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getFileDate() {
		return fileDate;
	}

	public void setFileDate(Date fileDate) {
		this.fileDate = fileDate;
	}
	
	
	
}