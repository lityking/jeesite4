/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.entity;

import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.modules.file.entity.FileEntity;
import com.jeesite.modules.file.entity.FileUpload;

/**
 * filePageEntity
 * @author system
 * @version 2018-06-13
 */
@Table(name="file_page", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="fileid", attrName="fileUpload.id", label="文件id"),
		@Column(name="filepage", attrName="filepage", label="页码"),
		@Column(name="remark", attrName="remark", label="备注"),
		@Column(name="makeTime", attrName="makeTime", label="制成时间"),
	} ,joinTable={
			 @JoinTable(type=Type.JOIN, entity=FileUpload.class, alias="f",
			            on="f.id = a.fileid ",
			            columns={@Column(includeEntity=FileUpload.class)}),
			 
			
	},orderBy="a.id DESC"
)
public class FilePage extends DataEntity<FilePage> {
	
	private static final long serialVersionUID = 1L;
	private FileUpload fileUpload;		// 文件id
	private Long filepage;		// 页码
	private String remark;		// 备注
	private Date makeTime;  	//制成时间
	
	
	
	@JsonFormat(pattern = "yyyy-MM-dd ")
	public Date getMakeTime() {
		return makeTime;
	}

	public void setMakeTime(Date makeTime) {
		this.makeTime = makeTime;
	}

	public FilePage() {
		this(null);
	}

	public FilePage(String id){
		super(id);
	}


	public FileUpload getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(FileUpload fileUpload) {
		this.fileUpload = fileUpload;
	}

	public Long getFilepage() {
		return filepage;
	}

	public void setFilepage(Long filepage) {
		this.filepage = filepage;
	}
	
	@Length(min=0, max=255, message="备注长度不能超过 255 个字符")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}