/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.entity;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * staffEntity
 * @author system
 * @version 2018-06-08
 */
@Table(name="staff", alias="a", columns={
		@Column(name="staff_code", attrName="staffCode", label="staff_code",queryType=QueryType.LIKE, isPK=true),
		@Column(name="staff_name", attrName="staffName", label="staff_name", queryType=QueryType.LIKE),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class Staff extends DataEntity<Staff> {
	
	private static final long serialVersionUID = 1L;
	private String staffCode;		// staff_code
	private String staffName;		// staff_name
	private String staffDeclare;    //员工名+员工工号
	
	
	public String getStaffDeclare() {
		return "姓名:"+staffName+"---"+"工号:"+staffCode;
	}

	public void setStaffDeclare(String staffDeclare) {
		this.staffDeclare = staffDeclare;
	}

	public Staff() {
		this(null);
	}

	public Staff(String id){
		super(id);
	}
	
	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}
	
	@Length(min=0, max=255, message="staff_name长度不能超过 255 个字符")
	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	
	
}