/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.dao;

import com.jeesite.common.dao.TreeDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.recordmanage.entity.RecordCatalogue;

/**
 * 档案目录DAO接口
 * @author system
 * @version 2018-06-11
 */
@MyBatisDao
public interface RecordCatalogueDao extends TreeDao<RecordCatalogue> {
	
}