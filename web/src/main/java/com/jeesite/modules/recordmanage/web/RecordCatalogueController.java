/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.web;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.collect.ListUtils;
import com.jeesite.common.collect.MapUtils;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.common.idgen.IdGen;
import com.jeesite.modules.sys.utils.UserUtils;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.recordmanage.entity.RecordCatalogue;
import com.jeesite.modules.recordmanage.service.RecordCatalogueService;

/**
 * 档案目录Controller
 * @author system
 * @version 2018-06-11
 */
@Controller
@RequestMapping(value = "${adminPath}/recordmanage/recordCatalogue")
public class RecordCatalogueController extends BaseController {

	@Autowired
	private RecordCatalogueService recordCatalogueService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public RecordCatalogue get(String treeCode, boolean isNewRecord) {
		return recordCatalogueService.get(treeCode, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("recordmanage:recordCatalogue:view")
	@RequestMapping(value = {"list", ""})
	public String list(RecordCatalogue recordCatalogue, Model model) {
		model.addAttribute("recordCatalogue", recordCatalogue);
		return "modules/recordmanage/recordCatalogueList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("recordmanage:recordCatalogue:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public List<RecordCatalogue> listData(RecordCatalogue recordCatalogue) {
		if (StringUtils.isBlank(recordCatalogue.getParentCode())) {
			recordCatalogue.setParentCode(RecordCatalogue.ROOT_CODE);
		}
		if (StringUtils.isNotBlank(recordCatalogue.getTreeName())){
			recordCatalogue.setParentCode(null);
		}
		if (StringUtils.isNotBlank(recordCatalogue.getStatus())){
			recordCatalogue.setParentCode(null);
		}
		if (StringUtils.isNotBlank(recordCatalogue.getRemarks())){
			recordCatalogue.setParentCode(null);
		}
		List<RecordCatalogue> list = recordCatalogueService.findList(recordCatalogue);
		return list;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("recordmanage:recordCatalogue:view")
	@RequestMapping(value = "form")
	public String form(RecordCatalogue recordCatalogue, Model model) {
		// 创建并初始化下一个节点信息
		recordCatalogue = createNextNode(recordCatalogue);
		model.addAttribute("recordCatalogue", recordCatalogue);
		return "modules/recordmanage/recordCatalogueForm";
	}
	
	/**
	 * 创建并初始化下一个节点信息，如：排序号、默认值
	 */
	@RequiresPermissions("recordmanage:recordCatalogue:edit")
	@RequestMapping(value = "createNextNode")
	@ResponseBody
	public RecordCatalogue createNextNode(RecordCatalogue recordCatalogue) {
		if (StringUtils.isNotBlank(recordCatalogue.getParentCode())){
			recordCatalogue.setParent(recordCatalogueService.get(recordCatalogue.getParentCode()));
		}
		if (recordCatalogue.getIsNewRecord()) {
			RecordCatalogue where = new RecordCatalogue();
			where.setParentCode(recordCatalogue.getParentCode());
			RecordCatalogue last = recordCatalogueService.getLastByParentCode(where);
			// 获取到下级最后一个节点
			if (last != null){
				recordCatalogue.setTreeSort(last.getTreeSort() + 30);
				recordCatalogue.setTreeCode(IdGen.nextCode(last.getTreeCode()));
			}else if (recordCatalogue.getParent() != null){
				recordCatalogue.setTreeCode(recordCatalogue.getParent().getTreeCode() + "001");
			}
		}
		// 以下设置表单默认数据
		if (recordCatalogue.getTreeSort() == null){
			recordCatalogue.setTreeSort(RecordCatalogue.DEFAULT_TREE_SORT);
		}
		return recordCatalogue;
	}

	/**
	 * 保存档案目录
	 */
	@RequiresPermissions("recordmanage:recordCatalogue:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated RecordCatalogue recordCatalogue) {
		recordCatalogueService.save(recordCatalogue);
		return renderResult(Global.TRUE, text("保存档案目录成功！"));
	}
	
	/**
	 * 删除档案目录
	 */
	@RequiresPermissions("recordmanage:recordCatalogue:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(RecordCatalogue recordCatalogue) {
		recordCatalogueService.delete(recordCatalogue);
		return renderResult(Global.TRUE, text("删除档案目录成功！"));
	}
	
	/**
	 * 获取树结构数据
	 * @param excludeCode 排除的Code
	 * @param isShowCode 是否显示编码（true or 1：显示在左侧；2：显示在右侧；false or null：不显示）
	 * @return
	 */
	@RequiresPermissions("recordmanage:recordCatalogue:view")
	@RequestMapping(value = "treeData")
	@ResponseBody
	public List<Map<String, Object>> treeData(String excludeCode, String isShowCode) {
		List<Map<String, Object>> mapList = ListUtils.newArrayList();
		List<RecordCatalogue> list = recordCatalogueService.findList(new RecordCatalogue());
		for (int i=0; i<list.size(); i++){
			RecordCatalogue e = list.get(i);
			// 过滤非正常的数据
			if (!RecordCatalogue.STATUS_NORMAL.equals(e.getStatus())){
				continue;
			}
			// 过滤被排除的编码（包括所有子级）
			if (StringUtils.isNotBlank(excludeCode)){
				if (e.getId().equals(excludeCode)){
					continue;
				}
				if (e.getParentCodes().contains("," + excludeCode + ",")){
					continue;
				}
			}
			Map<String, Object> map = MapUtils.newHashMap();
			map.put("id", e.getId());
			map.put("pId", e.getParentCode());
			map.put("name", StringUtils.getTreeNodeName(isShowCode, e.getTreeCode(), e.getTreeName()));
			mapList.add(map);
		}
		return mapList;
	}

	/**
	 * 修复表结构相关数据
	 */
	@RequiresPermissions("recordmanage:recordCatalogue:edit")
	@RequestMapping(value = "fixTreeData")
	@ResponseBody
	public String fixTreeData(RecordCatalogue recordCatalogue){
		if (!UserUtils.getUser().isAdmin()){
			return renderResult(Global.FALSE, "操作失败，只有管理员才能进行修复！");
		}
		recordCatalogueService.fixTreeData();
		return renderResult(Global.TRUE, "数据修复成功");
	}
	
}