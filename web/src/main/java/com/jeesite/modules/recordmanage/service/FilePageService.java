/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.recordmanage.entity.FilePage;
import com.jeesite.modules.recordmanage.dao.FilePageDao;

/**
 * filePageService
 * @author system
 * @version 2018-06-13
 */
@Service
@Transactional(readOnly=true)
public class FilePageService extends CrudService<FilePageDao, FilePage> {
	
	/**
	 * 获取单条数据
	 * @param filePage
	 * @return
	 */
	@Override
	public FilePage get(FilePage filePage) {
		return super.get(filePage);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param filePage
	 * @return
	 */
	@Override
	public Page<FilePage> findPage(Page<FilePage> page, FilePage filePage) {
		return super.findPage(page, filePage);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param filePage
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(FilePage filePage) {
		super.save(filePage);
	}
	
	/**
	 * 更新状态
	 * @param filePage
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(FilePage filePage) {
		super.updateStatus(filePage);
	}
	
	/**
	 * 删除数据
	 * @param filePage
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(FilePage filePage) {
		super.delete(filePage);
	}
	
}