/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.lang.DateUtils;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.file.entity.FileUpload;
import com.jeesite.modules.file.service.FileEntityService;
import com.jeesite.modules.file.service.FileUploadService;
import com.jeesite.modules.file.utils.FileUploadUtils;
import com.jeesite.modules.recordmanage.entity.FilePage;
import com.jeesite.modules.recordmanage.entity.RecordCatalogue;
import com.jeesite.modules.recordmanage.entity.Staff;
import com.jeesite.modules.recordmanage.entity.StaffRecord;
import com.jeesite.modules.recordmanage.service.FilePageService;
import com.jeesite.modules.recordmanage.service.RecordCatalogueService;
import com.jeesite.modules.recordmanage.service.StaffRecordService;
import com.jeesite.modules.recordmanage.service.StaffService;

/**
 * staffController
 * @author system
 * @version 2018-06-08
 */
@Controller
@RequestMapping(value = "${adminPath}/recordmanage/staff")
public class StaffController extends BaseController {

	@Autowired
	private StaffService staffService;
	@Autowired
	private RecordCatalogueService catalogueService;
	@Autowired
	private StaffRecordService recordService;
	@Autowired
	private FilePageService filePageService;
	@Autowired
	private FileUploadService fileUploadService;
	@Autowired
	private FileEntityService entityService;
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Staff get(String staffCode, boolean isNewRecord) {
		return staffService.get(staffCode, isNewRecord);
	}
	
	/**
	 * 跳转首页
	 */
	
	@RequestMapping(value = {"index"})
	public String index() {
		return "modules/recordmanage/sysDesktop";
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("recordmanage:staff:view")
	@RequestMapping(value = {"list", ""})
	public String list(Staff staff, Model model) {
		model.addAttribute("staff", staff);
		return "modules/recordmanage/staffList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("recordmanage:staff:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Staff> listData(Staff staff, HttpServletRequest request, HttpServletResponse response) {
		Page<Staff> page = staffService.findPage(new Page<Staff>(request, response), staff); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("recordmanage:staff:view")
	@RequestMapping(value = "form")
	public String form(Staff staff, Model model) {
		model.addAttribute("staff", staff);
		return "modules/recordmanage/staffForm";
	}
	/**
	 * 上传材料
	 */
	@RequiresPermissions("recordmanage:staff:view")
	@RequestMapping(value = "upLoad")
	public String upLoad(Staff staff, Model model) {
		RecordCatalogue recordCatalogue = new RecordCatalogue();
		recordCatalogue.setStatus("0");
		List<RecordCatalogue> list = catalogueService.findList(recordCatalogue );
		
		StaffRecord record = new StaffRecord();
		record.setStaff(staff);
		List<StaffRecord> records = recordService.findList(record );
		
		model.addAttribute("staff", staff);
		model.addAttribute("cats", list);
		model.addAttribute("records", records);
		
		return "modules/recordmanage/staffDeclare";
	}
	/**
	 * 材料清单
	 */
	@RequiresPermissions("recordmanage:staff:view")
	@RequestMapping(value = "listRecord")
	public String listRecord(Staff staff, Model model) {
		RecordCatalogue recordCatalogue = new RecordCatalogue();
		recordCatalogue.setStatus("0");
		List<RecordCatalogue> list = catalogueService.findList(recordCatalogue );
		
		StaffRecord record = new StaffRecord();
		record.setStaff(staff);
		List<StaffRecord> records = recordService.findList(record );
		
		for (StaffRecord staffRecord : records) {
			FileUpload entity = new FileUpload();
			entity.setBizKey(staffRecord.getId());
			FilePage filePage = new FilePage();
			filePage.setFileUpload(entity );
			//根据fileuploadId查询到是否已有数据
			List<FilePage> files = filePageService.findList(filePage);
			for (FilePage page : files) {
				page.setFileUpload(fileUploadService.get(page.getFileUpload()));
			}
			staffRecord.setFiles(files);
		}
		
		model.addAttribute("staff", staff);
		model.addAttribute("cats", list);
		model.addAttribute("records", records);
		
		return "modules/recordmanage/staffRecordDetail";
	}

	/**
	 * 保存staff
	 */
	@RequiresPermissions("recordmanage:staff:view")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Staff staff,String isNewRecord,Model model) {
		staffService.save(staff);
		if(isNewRecord.equals("true")){
			//新增的话就传员工的id返回给js进行跳转
			return renderResult(Global.FALSE, text(staff.getStaffCode()));
		}else{
			return renderResult(Global.TRUE, text("保存员工成功！"));
		}
	}
	
	/**
	 * 删除staff
	 */
	@RequiresPermissions("recordmanage:staff:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Staff staff) {
		staffService.delete(staff);
		return renderResult(Global.TRUE, text("删除员工成功！"));
	}
	/**
	 * 保存附件
	 */
	@RequiresPermissions("recordmanage:staff:edit")
	@RequestMapping(value = "saveRecords")
	@ResponseBody
	public String saveRecords(Staff staff,HttpServletRequest request) {
		RecordCatalogue recordCatalogue = new RecordCatalogue();
		Map<String, String[]> parameterMap = request.getParameterMap();
		for(String s:parameterMap.keySet()){
			//key开头的就是附件
			if(s.startsWith("key")&&parameterMap.get(s).length>0&&StringUtils.isNotEmpty(parameterMap.get(s)[0])){
				StaffRecord staffRecord = new StaffRecord();
				staffRecord.setStaff(staff);
				recordCatalogue.setId(s.replace("key", ""));
				staffRecord.setRecordCatalogue(recordCatalogue);
				List<StaffRecord> list = recordService.findList(staffRecord);
				//判读是否已有该信息有的话直接保存,没有的话新增后保存
				if(list.size()>0){
					FileUploadUtils.saveFileUpload(list.get(0).getId(), s);
				}else{
					recordService.save(staffRecord);
					FileUploadUtils.saveFileUpload(staffRecord.getId(), s);
				}
			}
			if(s.startsWith("page")||s.startsWith("time")||s.startsWith("rema")){
				
				FilePage filePage = new FilePage();
				FileUpload fileUpload = new FileUpload();
				fileUpload.setId(s.substring(4));
				filePage.setFileUpload(fileUpload );
				//根据fileuploadId查询到是否已有数据
				List<FilePage> filepages = filePageService.findList(filePage);
				if(filepages.size()>0){
					filePage=filepages.get(0);
				}
				if(parameterMap.get(s).length>0&&StringUtils.isNotEmpty(parameterMap.get(s)[0])){
					if(s.startsWith("page")){
						filePage.setFilepage(Long.parseLong(parameterMap.get(s)[0]));
					}else if(s.startsWith("time")){
						filePage.setMakeTime(DateUtils.parseDate(parameterMap.get(s)[0]));
						
					}else if(s.startsWith("rema")){
						filePage.setRemark(parameterMap.get(s)[0]);
					}
				}else{
					filePage.setFilepage(0L);
				}
				
				filePageService.save(filePage );
			}
     }
		
		
		
		return renderResult(Global.TRUE, text("添加成功！"));
	}
	
	
	
}