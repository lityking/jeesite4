/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.recordmanage.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.file.entity.FileUpload;
import com.jeesite.modules.file.service.FileUploadService;
import com.jeesite.modules.file.utils.FileUploadUtils;
import com.jeesite.modules.recordmanage.entity.FilePage;
import com.jeesite.modules.recordmanage.entity.RecordCatalogue;
import com.jeesite.modules.recordmanage.entity.Staff;
import com.jeesite.modules.recordmanage.entity.StaffRecord;
import com.jeesite.modules.recordmanage.service.FilePageService;
import com.jeesite.modules.recordmanage.service.RecordCatalogueService;
import com.jeesite.modules.recordmanage.service.StaffRecordService;
import com.jeesite.modules.recordmanage.service.StaffService;

/**
 * staffRecordController
 * @author system
 * @version 2018-06-11
 */
@Controller
@RequestMapping(value = "${adminPath}/recordmanage/staffRecord")
public class StaffRecordController extends BaseController {

	@Autowired
	private StaffRecordService staffRecordService;
	@Autowired
	private StaffService staffService;
	@Autowired
	private RecordCatalogueService recordCatalogueService;
	
	@Autowired
	private FileUploadService fileUploadService;
	
	@Autowired
	private FilePageService filePageService;
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public StaffRecord get(String id, boolean isNewRecord) {
		return staffRecordService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("recordmanage:staffRecord:view")
	@RequestMapping(value = {"list", ""})
	public String list(StaffRecord staffRecord, Model model) {
		//下拉框所需要的目录和员工list
		Staff staff = new Staff();
		staff.setStatus("0");
		List<Staff> staffList = staffService.findList(staff );
		RecordCatalogue recordCatalogue = new RecordCatalogue();
		recordCatalogue.setStatus("0");
		List<RecordCatalogue> catList = recordCatalogueService.findList(recordCatalogue );
		
		model.addAttribute("staffRecord", staffRecord);
		model.addAttribute("staffList", staffList);
		model.addAttribute("catList", catList);
		return "modules/recordmanage/staffRecordList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("recordmanage:staffRecord:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<StaffRecord> listData(StaffRecord staffRecord, HttpServletRequest request, HttpServletResponse response) {
		Page<StaffRecord> page = staffRecordService.findPage(new Page<StaffRecord>(request, response), staffRecord); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("recordmanage:staffRecord:view")
	@RequestMapping(value = "form")
	public String form(StaffRecord staffRecord, Model model) {
		//下拉框所需要的目录和员工list
				Staff staff = new Staff();
				staff.setStatus("0");
				List<Staff> staffList = staffService.findList(staff );
				RecordCatalogue recordCatalogue = new RecordCatalogue();
				recordCatalogue.setStatus("0");
				List<RecordCatalogue> catList = recordCatalogueService.findList(recordCatalogue );
				
				model.addAttribute("staffRecord", staffRecord);
				model.addAttribute("staffList", staffList);
				model.addAttribute("catList", catList);
		return "modules/recordmanage/staffRecordForm";
	}

	/**
	 * 保存员工文件中间表
	 */
	@RequiresPermissions("recordmanage:staffRecord:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated StaffRecord staffRecord) {
		staffRecordService.save(staffRecord);
		FileUploadUtils.saveFileUpload(staffRecord.getId(), "staffRecord_file");
		return renderResult(Global.TRUE, text("保存员工文件信息成功！"));
	}
	
	/**
	 * 删除员工文件中间表
	 */
	@RequiresPermissions("recordmanage:staffRecord:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(StaffRecord staffRecord) {
		staffRecordService.delete(staffRecord);
		return renderResult(Global.TRUE, text("删除员工文件信息成功！"));
	}
	/**
	 * 修改页码值
	 */
	@RequestMapping(value = "editPage")
	@ResponseBody
	public String editPage(FileUpload fileUpload,String filePage) {
		FilePage filepage = new FilePage();
		filepage.setFileUpload(fileUpload);
		if(StringUtils.isNotEmpty(filePage)){
			filepage.setFilepage(Long.parseLong(filePage));
		}
		filePageService.save(filepage );
		return "SUCCESSFUL";
	}
	/**
	 * 根据fileUploadId查询页码
	 */
	@RequestMapping(value = "findPage")
	@ResponseBody
	public FilePage findPage(FileUpload fileUpload) {
		
		FilePage page = new FilePage();
		page.setFileUpload(fileUpload);
		List<FilePage> list = filePageService.findList(page );
		if(list.size()>0){
			if(list.get(0).getFilepage()!=null){
				return list.get(0);
			}
		}
		return page;
	}
}